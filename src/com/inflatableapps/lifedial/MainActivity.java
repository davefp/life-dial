package com.inflatableapps.lifedial;

import java.util.ArrayList;
import java.util.List;

import net.robotmedia.billing.helper.AbstractBillingObserver;
import net.robotmedia.billing.BillingController;
import net.robotmedia.billing.BillingRequest.ResponseCode;

import ca.carsonbrown.hci.dialview.DialView;
import ca.carsonbrown.hci.dialview.DialView.DialListener;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.inflatableapps.lifedial.dialogs.PaidFeatureDialogFragment;
import com.inflatableapps.lifedial.dialogs.ReviewPromptDialogFragment;
import com.inflatableapps.lifedial.dialogs.RollDialogFragment;
import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.TitlePageIndicator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.widget.TextView;
import android.widget.Toast;

import net.robotmedia.billing.model.Transaction;
import net.robotmedia.billing.model.Transaction.PurchaseState;

public class MainActivity extends SherlockFragmentActivity implements
		DialListener, BillingController.IConfiguration, IUpgradable {

	private AbstractBillingObserver billingObserver;

	private static final int RESET_DIALOG = 0;
	private static final int PURCHASE_DIALOG = 1;

	public static final String TAG = "Life Dial";

	private DialView dial;
	private TextView deltaText;
	private int currentDelta;

	private boolean premium;
	private boolean billingSupported;

	PlayerTabsAdapter adapter;
	ViewPager pager;
	PageIndicator indicator;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BillingController.setConfiguration(this);
		billingObserver = new AbstractBillingObserver(this) {

			public void onBillingChecked(boolean supported) {
				MainActivity.this.onBillingChecked(supported);
			}

			public void onPurchaseStateChanged(String itemId,
					PurchaseState state) {
				MainActivity.this.onPurchaseStateChanged(itemId, state);
			}

			public void onRequestPurchaseResponse(String itemId,
					ResponseCode response) {
				MainActivity.this.onRequestPurchaseResponse(itemId, response);
			}

			public void onSubscriptionChecked(boolean supported) {
				MainActivity.this.onSubscriptionChecked(supported);
			}

		};
		setContentView(R.layout.main);

		// Marshal view components
		dial = (DialView) findViewById(R.id.dial);
		deltaText = (TextView) findViewById(R.id.diff_text);
		deltaText.setText("");

		dial.setCurrentValue(0);

		adapter = new PlayerTabsAdapter(getSupportFragmentManager(), this);

		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);

		indicator = (TitlePageIndicator) findViewById(R.id.indicator);
		indicator.setViewPager(pager);

		pager.setCurrentItem(0);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		BillingController.registerObserver(billingObserver);
		BillingController.checkBillingSupported(this);

		premium = false;
		billingSupported = true;
		updateOwnedItems();
		ReviewPromptDialogFragment.showIfAllowed(this, 7, 7);
	}

	protected void onSubscriptionChecked(boolean supported) {
	}

	protected void onRequestPurchaseResponse(String itemId,
			ResponseCode response) {
	}

	protected void onPurchaseStateChanged(String itemId, PurchaseState state) {
		updateOwnedItems();
	}

	protected void onBillingChecked(boolean supported) {
		restoreTransactions();
		billingSupported = supported;
	}

	private void restoreTransactions() {
		if (!billingObserver.isTransactionsRestored()) {
			BillingController.restoreTransactions(this);
			Toast.makeText(this, R.string.restoring_transactions,
					Toast.LENGTH_LONG).show();
		}
	}

	public void onDialPositionChanged(DialView sender, int delta) {
		getCurrentPlayer().updateTotal(delta);
		currentDelta += delta;
		if (currentDelta > 0) {
			deltaText.setText("+" + currentDelta);
		} else if (currentDelta < 0) {
			deltaText.setText("" + currentDelta);
		} else {
			deltaText.setText("0");
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		dial.addListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		dial.removeListener(this);
	}

	@Override
	protected void onDestroy() {
		BillingController.unregisterObserver(billingObserver);
		super.onDestroy();
	}

	public void onRotationEnd(DialView sender) {
		deltaText.setText("");
	}

	public void onRotationStart(DialView sender) {
		currentDelta = 0;
		deltaText.setText(String.valueOf(currentDelta));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.reset:
			showDialog(RESET_DIALOG);
			return true;
		case R.id.card_search:
			onSearchRequested();
			return true;
		case R.id.buy:
			showDialog(PURCHASE_DIALOG);
			return true;
		case R.id.roll:
			roll();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void roll() {
		if (isUpgraded()) {
			SherlockDialogFragment newFragment = RollDialogFragment
					.newInstance();
			newFragment.show(getSupportFragmentManager(), TAG);
		} else {
			SherlockDialogFragment paidFeatureFragment = PaidFeatureDialogFragment.newInstance();
			paidFeatureFragment.show(getSupportFragmentManager(), TAG);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case RESET_DIALOG:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.reset_confirmation)
					.setTitle(R.string.reset_title)
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									MainActivity.this.reset();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			return builder.create();
		case PURCHASE_DIALOG:
			builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.purchase_confirmation)
					.setTitle(R.string.purchase_title)
					.setCancelable(false)
					.setPositiveButton("Purchase",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									MainActivity.this.initiatePurchase();
								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			return builder.create();
		default:
			return null;
		}
	}

	private void reset() {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();

		for (int i = 0; i < adapter.getCount(); ++i) {
			Fragment f = adapter.getActiveFragment(pager, i);
			if (f != null) {
				transaction.remove(f);
			}
		}

		transaction.commit();
		pager.setAdapter(adapter);
		indicator.setCurrentItem(0);
	}

	private PlayerFragment getCurrentPlayer() {
		return adapter.getActiveFragment(pager, pager.getCurrentItem());
	}

	public ArrayList<Integer> getStartingTotals() {
		ArrayList<Integer> startingTotals = new ArrayList<Integer>();
		startingTotals.add(20);
		startingTotals.add(0);
		startingTotals.add(0);
		return startingTotals;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (premium || !billingSupported)
			menu.removeItem(R.id.buy);
		return true;
	}

	private void initiatePurchase() {
		BillingController.requestPurchase(MainActivity.this,
				getString(R.string.upgrade_key), true /* confirm */,
				"Developer Payload");
	}

	public byte[] getObfuscationSalt() {
		return new byte[] { 75, 95, -9, 72, -127, -124, -73, -42, 14, 68, 19,
				13, 116, -123, -17, -106, 36, 33, -12, -102 };
	}

	public String getPublicKey() {
		return getString(R.string.public_key);
	}

	private void updateOwnedItems() {
		List<Transaction> transactions = BillingController
				.getTransactions(this);
		final ArrayList<String> ownedItems = new ArrayList<String>();
		for (Transaction t : transactions) {
			if (t.purchaseState == PurchaseState.PURCHASED) {
				ownedItems.add(t.productId);
			}
		}

		if (ownedItems.contains(getString(R.string.upgrade_key))) {
			premium = true;
			invalidateOptionsMenu();
			reset();
		}
	}

	@Override
	public boolean isUpgraded() {
		return premium;
	}
}