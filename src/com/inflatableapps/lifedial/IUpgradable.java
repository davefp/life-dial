package com.inflatableapps.lifedial;

public interface IUpgradable {
	public boolean isUpgraded();

}
