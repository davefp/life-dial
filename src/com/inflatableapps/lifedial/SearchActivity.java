package com.inflatableapps.lifedial;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class SearchActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.search);

		// Get the intent, verify the action and get the query
		Intent intent = getIntent();
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			cardSearch(query);
		}
	}
	

	
	private void cardSearch(String query) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		String[] arr = query.split("\\s+");
		String q = "";
		for (String s : arr) {
			q += "+[" + s + "]";
		}
		Uri data = Uri.parse("http://gatherer.wizards.com/Pages/Search/Default.aspx?name=" + q);
		intent.setData(data);
		startActivity(intent);
		finish();
	}

}
