package com.inflatableapps.lifedial;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class PlayerFragment extends Fragment implements OnClickListener {

	private ArrayList<Integer> totals;
	private int activeTotal;
	private TextView activeText;
	private LinearLayout layout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		totals = ((MainActivity) getActivity()).getStartingTotals();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt("activeTotal", activeTotal);
		outState.putIntegerArrayList("totals", totals);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			activeTotal = savedInstanceState.getInt("activeTotal");
			totals = savedInstanceState.getIntegerArrayList("totals");
		}
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = new LinearLayout(getActivity());
		layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));

		createTotals();
		return layout;
	}

	public void updateTotal(int delta) {
		totals.set(activeTotal, totals.get(activeTotal) + delta);
		activeText.setText(totals.get(activeTotal).toString());
	}

	public void onClick(View v) {
		setActiveText((TextView) v);
	}

	private void setActiveText(TextView v) {

		if (activeText != null) {
			activeText.setTextColor(getResources().getColor(
					android.R.color.secondary_text_dark));
		}

		activeText = v;
		activeTotal = Integer.parseInt(activeText.getTag().toString());

		activeText.setTextColor(getResources().getColor(
				android.R.color.primary_text_dark));
	}
	
	public void reset(ArrayList<Integer> totals) {
		this.totals = totals;
		createTotals();
	}

	private void createTotals() {
		layout.removeAllViews();
		
		for (int i = 0; i < totals.size(); ++i) {
			Integer total = totals.get(i);
			TextView view = new TextView(getActivity());
			view.setLayoutParams(new TableLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
			view.setText(total.toString());
			view.setTextSize(64.0f);
			view.setGravity(Gravity.CENTER);
			view.setOnClickListener(this);
			view.setTag(i);
			layout.addView(view);
		}
		setActiveText((TextView) layout.getChildAt(0));

	}

}
