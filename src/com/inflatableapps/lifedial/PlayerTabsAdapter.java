package com.inflatableapps.lifedial;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class PlayerTabsAdapter extends FragmentPagerAdapter {

	FragmentManager fragmentManager;
	IUpgradable parent;

	public PlayerTabsAdapter(FragmentManager fm, IUpgradable parent) {
		super(fm);
		fragmentManager = fm;
		this.parent = parent;
	}

	@Override
	public Fragment getItem(int position) {
		return new PlayerFragment();
	}

	@Override
	public int getCount() {
		if (parent.isUpgraded()) {
			return 10;
		} else {
			return 1;
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return "Player " + (position + 1);
	}

	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return super.getItemPosition(object);
	}

	public PlayerFragment getActiveFragment(ViewPager container, int position) {
		String name = makeFragmentName(container.getId(), position);
		return (PlayerFragment) fragmentManager.findFragmentByTag(name);
	}

	public static String makeFragmentName(int viewId, int index) {
		return "android:switcher:" + viewId + ":" + index;
	}
}
