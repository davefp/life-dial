package com.inflatableapps.lifedial.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.inflatableapps.lifedial.R;

public class ReviewPromptDialogFragment extends SherlockDialogFragment {

	private final static String APP_PNAME = "com.inflatableapps.lifedial";
	private final static String ALLOWED = "allowed";
	private final static String TAG = "LifeDial";

	public static boolean showIfAllowed(SherlockFragmentActivity activity,
			int days, int launches) {
		SharedPreferences prefs = activity.getSharedPreferences("rate_me",
				Context.MODE_PRIVATE);
		boolean allowed = prefs.getBoolean(ALLOWED, true);
		if (allowed) {
			SharedPreferences.Editor editor = prefs.edit();

			// Increment launch counter
			long launch_count = prefs.getLong("launch_count", 0) + 1;
			editor.putLong("launch_count", launch_count);

			// Get date of first launch
			Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
			if (date_firstLaunch == 0) {
				date_firstLaunch = System.currentTimeMillis();
				editor.putLong("date_firstlaunch", date_firstLaunch);
			}
			if (launch_count >= launches
					&& System.currentTimeMillis() >= date_firstLaunch
							+ (days * 24 * 60 * 60 * 1000)) {
				newInstance().show(activity.getSupportFragmentManager(), TAG);
			}

			editor.commit();
		}
		return allowed;
	}

	private static ReviewPromptDialogFragment newInstance() {
		ReviewPromptDialogFragment frag = new ReviewPromptDialogFragment();
		return frag;
	}

	private void setAllowShow(boolean allowed) {
		SharedPreferences prefs = getActivity().getSharedPreferences("rate_me",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(ALLOWED, allowed);
		editor.commit();

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Activity activity = getActivity();

		return new AlertDialog.Builder(activity)
				.setMessage(R.string.review_prompt_message)
				.setTitle(R.string.review_prompt_title)
				.setNeutralButton(R.string.review_prompt_neutral,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						})
				.setPositiveButton(R.string.review_prompt_positive,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								setAllowShow(false);
								activity.startActivity(new Intent(
										Intent.ACTION_VIEW, Uri
												.parse("market://details?id="
														+ APP_PNAME)));
								dialog.dismiss();
							}
						})
				.setNegativeButton(R.string.review_prompt_negative,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								setAllowShow(false);
								dialog.dismiss();
							}
						}).create();
	}

}
