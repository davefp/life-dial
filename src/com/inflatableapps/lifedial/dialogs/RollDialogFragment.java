package com.inflatableapps.lifedial.dialogs;

import java.util.Random;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.inflatableapps.lifedial.R;

public class RollDialogFragment extends SherlockDialogFragment implements OnClickListener {

	private Random random;

	TextView d4;
	TextView d6;
	TextView d8;
	TextView d12;
	TextView d20;

	public static RollDialogFragment newInstance() {
		RollDialogFragment frag = new RollDialogFragment();
		return frag;
	}

	public RollDialogFragment() {
		super();
		random = new Random(System.currentTimeMillis());
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Context context = getActivity();
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.roll_dialog, null);

		d4 = (TextView) v.findViewById(R.id.d4);
		d4.setText(Integer.toString(1 + random.nextInt(4)));
		d4.setOnClickListener(this);

		d6 = (TextView) v.findViewById(R.id.d6);
		d6.setText(Integer.toString(1 + random.nextInt(6)));
		d6.setOnClickListener(this);

		d8 = (TextView) v.findViewById(R.id.d8);
		d8.setText(Integer.toString(1 + random.nextInt(8)));
		d8.setOnClickListener(this);

		d12 = (TextView) v.findViewById(R.id.d12);
		d12.setText(Integer.toString(1 + random.nextInt(12)));
		d12.setOnClickListener(this);

		d20 = (TextView) v.findViewById(R.id.d20);
		d20.setText(Integer.toString(1 + random.nextInt(20)));
		d20.setOnClickListener(this);

		return new AlertDialog.Builder(context)
				.setView(v)
				.setTitle(R.string.roll_title)
				.setNeutralButton("Close",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						}).create();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.d4:
			d4.setText(Integer.toString(1 + random.nextInt(4)));
			return;
		case R.id.d6:
			d6.setText(Integer.toString(1 + random.nextInt(6)));
			return;
		case R.id.d8:
			d8.setText(Integer.toString(1 + random.nextInt(8)));
			return;
		case R.id.d12:
			d12.setText(Integer.toString(1 + random.nextInt(12)));
			return;
		case R.id.d20:
			d20.setText(Integer.toString(1 + random.nextInt(20)));
			return;
		}
	}
}