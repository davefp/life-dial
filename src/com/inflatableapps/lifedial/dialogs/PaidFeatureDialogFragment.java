package com.inflatableapps.lifedial.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.inflatableapps.lifedial.R;

public class PaidFeatureDialogFragment extends SherlockDialogFragment {

	private final static String TAG = "LifeDial";
	private static final int PURCHASE_DIALOG = 1;


	public static PaidFeatureDialogFragment newInstance() {
		PaidFeatureDialogFragment frag = new PaidFeatureDialogFragment();
		return frag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Activity activity = getActivity();

		return new AlertDialog.Builder(activity)
				.setMessage(R.string.paid_feature_message)
				.setTitle(R.string.paid_feature_title)
				.setPositiveButton(R.string.paid_feature_positive,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								activity.showDialog(PURCHASE_DIALOG);
								dialog.dismiss();
							}
						})
				.setNegativeButton(R.string.paid_feature_negative,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						}).create();
	}

}
