package ca.carsonbrown.hci.dialview;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;

public class DialModel {
	public interface Listener {
		void onDialPositionChanged(DialModel sender, int totalNicks,
				int deltaNicks);
	}

	private List<Listener> listeners = new ArrayList<Listener>();

	private int totalNicks;
	private int currentNick;
	private int currentValue;

	public DialModel(int totalNicks) {
		this(totalNicks, 0);
	}

	public DialModel(int totalNicks, int currentNick) {
		this.totalNicks = totalNicks;
		this.currentNick = currentNick;
		this.currentValue = 0;
	}

	public void reset() {
		currentNick = 0;
		currentValue = 0;
		for (Listener listener : listeners) {
			listener.onDialPositionChanged(this, currentValue, 0);
		}
	}

	public final float getRotationInDegrees() {
		return (360.0f / totalNicks) * currentNick;
	}

	public final void rotate(int nicks) {
		currentNick = (currentNick + nicks);
		if (currentNick >= totalNicks) {
			currentNick %= totalNicks;
		} else if (currentNick < 0) {
			currentNick = (totalNicks + currentNick);
		}
		currentValue += nicks;

		for (Listener listener : listeners) {
			listener.onDialPositionChanged(this, currentValue, nicks);
		}
	}

	public final List<Listener> getListeners() {
		return listeners;
	}

	public final int getTotalNicks() {
		return totalNicks;
	}

	public final int getCurrentNick() {
		return currentNick;
	}

	public final void addListener(Listener listener) {
		listeners.add(listener);
	}

	public final void removeListener(Listener listener) {
		listeners.remove(listener);
	}

	private static String getBundlePrefix() {
		return DialModel.class.getSimpleName() + ".";
	}

	public final void save(Bundle bundle) {
		String prefix = getBundlePrefix();

		bundle.putInt(prefix + "totalNicks", totalNicks);
		bundle.putInt(prefix + "currentNick", currentNick);
	}

	public static DialModel restore(Bundle bundle) {
		String prefix = getBundlePrefix();
		DialModel model = new DialModel(bundle.getInt(prefix + "totalNicks"),
				bundle.getInt(prefix + "currentNick"));
		return model;
	}

	public int getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(int currentValue) {
		this.currentValue = currentValue;
	}
}
