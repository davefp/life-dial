package ca.carsonbrown.hci.dialview;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import com.inflatableapps.lifedial.R;

public class DialView extends View implements DialModel.Listener,
		OnGestureListener {

	public interface DialListener {
		void onDialPositionChanged(DialView sender, int deltaNicks);

		void onRotationStart(DialView sender);

		void onRotationEnd(DialView sender);
	}

	private ArrayList<DialListener> listeners;

	// private static final String TAG = DialView.class.getSimpleName();

	private static boolean toolsInitialized = false;
	private static Rect bounds;
	private static Bitmap texture;
	private static Paint texturePaint;

	// private static SoundPool soundPool;
	// private static int clickSoundId;

	private static Paint createDefaultPaint() {
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		return paint;
	}

	/*
	 * private static void initSoundPoolIfNecessary(Context context) { if
	 * (soundPool == null) { soundPool = new SoundPool(1,
	 * AudioManager.STREAM_SYSTEM, 0); clickSoundId = soundPool.load(context,
	 * R.raw.click_sound, 1); } }
	 */

	private static void initDrawingToolsIfNecessary(Context context) {
		if (!toolsInitialized) {
			bounds = new Rect();

			// there's a subtle thing here. technically, different instances
			// of DialView might use different contexts. however, what we are
			// creating here is a Bitmap which is not bound to any context.
			texture = BitmapFactory.decodeResource(context.getResources(),
					R.drawable.dial);
			texturePaint = createDefaultPaint();
			BitmapShader textureShader = new BitmapShader(texture,
					TileMode.MIRROR, TileMode.MIRROR);
			Matrix textureMatrix = new Matrix();
			textureMatrix.setScale(1.0f / texture.getWidth(), 1.0f / texture
					.getHeight());
			textureShader.setLocalMatrix(textureMatrix);
			texturePaint.setShader(textureShader);
			texturePaint.setDither(true);
			toolsInitialized = true;
		}
	}

	private GestureDetector gestureDetector;
	private float dragStartDeg = Float.NaN;

	private DialModel model;
	private Handler handler;

	private DrawLayer outerLayer;

	public DialView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public DialView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	private void init(AttributeSet attrs) {
		listeners = new ArrayList<DialListener>();

		initDrawingToolsIfNecessary(getContext());

		gestureDetector = new GestureDetector(getContext(), this);
		TypedArray a = getContext().obtainStyledAttributes(attrs,
				R.styleable.DialView);
		setModel(new DialModel(a.getInt(R.styleable.DialView_total_nicks, 20)));
		handler = new Handler();

		outerLayer = new DrawLayer();
	}

	public final void setModel(DialModel model) {
		if (this.model != null) {
			this.model.removeListener(this);
		}
		this.model = model;
		this.model.addListener(this);

		invalidate();
	}

	public final DialModel getModel() {
		return model;
	}

	@Override
	protected final void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		SquareViewMeasurer measurer = new SquareViewMeasurer(100);
		measurer.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(measurer.getChosenDimension(), measurer
				.getChosenDimension());
	}

	private float getBaseRadius() {
		return 0.50f; // to avoid some aliasing issues
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.getClipBounds(bounds);

		canvas.save(Canvas.MATRIX_SAVE_FLAG);
		{
			canvas.translate(bounds.left, bounds.top);

			float rotation = model.getRotationInDegrees();
			float midX = bounds.width() / 2.0f;
			float midY = bounds.height() / 2.0f;

			canvas.rotate(rotation, midX, midY);
			int minDim = Math.min(bounds.width(),bounds.height());
			int startX = (bounds.width() - minDim)/2;
			int startY = (bounds.height() - minDim)/2;
			
			outerLayer.drawOn(canvas, startX, startY);
			canvas.rotate(-rotation, midX, midY);
		}
		canvas.restore();
	}

	private void drawOuterCircle(Canvas canvas, float baseRadius) {
		canvas.drawCircle(0.5f, 0.5f, baseRadius, texturePaint);
	}

	public void onDialPositionChanged(DialModel sender, int nicksChanged,
			int delta) {
		invalidate();

		for (DialListener l : listeners) {
			l.onDialPositionChanged(this, delta);
		}

		// playSound(nicksChanged);
	}

	/*
	 * private void playSound(int nicksChanged) { int sounds = Math.min(3,
	 * Math.abs(nicksChanged)); for (int i = 0; i < sounds; ++i) {
	 * soundPool.play(clickSoundId, 0.03f, 0.03f, 0, 0, 1.0f);
	 * SystemClock.sleep(10); } }
	 */

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Bundle bundle = (Bundle) state;
		super.onRestoreInstanceState(bundle.getParcelable("superState"));

		setModel(DialModel.restore(bundle));
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		Bundle bundle = new Bundle();
		bundle.putParcelable("superState", superState);

		model.save(bundle);

		return bundle;
	}

	private float xyToDegrees(float x, float y) {
		return (float) Math.toDegrees(Math.atan2(x - 0.5f, y - 0.5f));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gestureDetector.onTouchEvent(event)) {
			return true;
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			for(DialListener l : listeners) {
				l.onRotationEnd(this);
			}
			return true;
		} else {
			return super.onTouchEvent(event);
		}
	}

	public boolean onDown(MotionEvent event) {
		float x = event.getX() / ((float) getWidth());
		float y = event.getY() / ((float) getHeight());

		dragStartDeg = xyToDegrees(x, y);

		for (DialListener l : listeners) {
			l.onRotationStart(this);
		}

		return !Float.isNaN(dragStartDeg);
	}

	public boolean onFling(MotionEvent eventA, MotionEvent eventB, float vx,
			float vy) {
		return false;
	}

	public void onLongPress(MotionEvent event) {
	}

	public boolean onScroll(MotionEvent eventA, MotionEvent eventB, float dx,
			float dy) {
		if (Float.isNaN(dragStartDeg)) {
			return false;
		}
		float currentDeg = xyToDegrees(eventB.getX() / getWidth(), eventB
				.getY()
				/ getHeight());

		if (Float.isNaN(currentDeg)) {
			return true;
		}
		float degPerNick = 360.0f / model.getTotalNicks();
		float deltaDeg = dragStartDeg - currentDeg;
		if (Math.abs(deltaDeg) > 270.0f) {
			deltaDeg = -Math.signum(deltaDeg) * (360.0f - Math.abs(deltaDeg));
		}

		final int nicks = (int) (Math.signum(deltaDeg) * FloatMath.floor(Math
				.abs(deltaDeg)
				/ degPerNick));

		if (nicks != 0) {
			dragStartDeg = currentDeg;

			handler.post(new Runnable() {
				public void run() {
					model.rotate(nicks);
				}
			});
		}

		return true;
	}

	public void onShowPress(MotionEvent event) {

	}

	public boolean onSingleTapUp(MotionEvent event) {
		return false;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		outerLayer.onSizeChange(w, h);

		regenerateLayers(Math.min(w,h));
	}

	private void regenerateLayers(int size) {
		float baseRadius = getBaseRadius();

		float scale = (float) size;

		Canvas canvas = outerLayer.getCanvas();
		canvas.scale(scale, scale);
		drawOuterCircle(canvas, baseRadius);
	}

	public int getCurrentValue() {
		return model.getCurrentValue();
	}

	public void setCurrentValue(int currentValue) {
		model.setCurrentValue(currentValue);
	}

	public void addListener(DialListener listener) {
		listeners.add(listener);
	}

	public void removeListener(DialListener listener) {
		listeners.remove(listener);
	}
}
